import { TabsList } from './tabs.constant';
import { Tabs } from './tabs.interfaces';

export class TabsController {
  static $inject = ['TabsProperties'];
  tabs: Tabs['tabs'];
  tabsList: any;

  constructor (tabsList: TabsList) {
    this.tabsList = tabsList;
  }
  
  $onInit() {
    this.tabs = this.tabsList;
   }
}
