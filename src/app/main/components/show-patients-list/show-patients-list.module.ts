import * as angular from 'angular';
import { showPatientsListComponent } from './show-patients-list.component';

export const showPatientsListModule = angular
  .module('app.show-patients-list', [])
  .component('showPatientsListComponent', showPatientsListComponent)
  .name;
