import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import * as Rx from "rxjs/Rx";
import * as _ from "lodash";

export class SearchService {
  static $inject = ['$http'];
  patients$;

  constructor(private $http) {
  }

  filterPatient(dataArray, searchString) {
    const term = _.lowerCase(searchString);
    return _.filter(dataArray, function (object) {
        const termInFirstName = _.includes(_.lowerCase(object.firstName), term);
        const termInLastName = _.includes(_.lowerCase(object.lastName), term);
        return termInFirstName || termInLastName;
    });
  }

}
