import * as angular from 'angular';
import { medsModule } from './meds/meds.module';
import { problemsModule } from './problems/problems.module';
import { tabsModule } from './nav-tabs/tabs.module';
import { patientInfoModule } from './patient-info/patient-info.module';
import { widgetsSettingsModule } from './widgets-settings/widgets-settings.module';
import { showPatientsListModule } from './show-patients-list/show-patients-list.module';
import {searchModule} from './patient-search/patient-search.module'
import {headerModule} from './header/header.module'


export const componentsModule = angular
  .module('app.main.components', [
    tabsModule,
    medsModule,
    problemsModule,
    patientInfoModule,
    widgetsSettingsModule,      
    showPatientsListModule,    
    searchModule,
    headerModule
  ])
  .name;
