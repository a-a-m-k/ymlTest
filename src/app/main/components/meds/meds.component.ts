import './meds.component.scss';

import { MedsController } from './meds.controller';

export const MedsComponent: angular.IComponentOptions = {
  template: require('./meds.component.html'),
  controller: MedsController
};
