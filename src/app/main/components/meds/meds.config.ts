medsRoutes.$inject = ['$stateProvider'];

export function medsRoutes($stateProvider: any) {
  $stateProvider.state('meds', {
    url: '/meds',
    component: 'medsComponent'
  });
}
