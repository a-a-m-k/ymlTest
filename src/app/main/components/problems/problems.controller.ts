import { ProblemsService } from './problems.service';

export class ProblemsController {
  public static $inject = ['ProblemsService'];

  constructor(
    private problemsService: ProblemsService
  ) {};

}
