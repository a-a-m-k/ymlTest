problemsRoutes.$inject = ['$stateProvider'];

export function problemsRoutes($stateProvider: any) {
  $stateProvider.state('problems', {
    url: '/problems',
    component: 'problemsComponent'
  });
}
