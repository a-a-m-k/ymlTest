import { PatientInfoInterface } from './patient-info.interface';

export class PatientInfoController {
  patientInfo: PatientInfoInterface; 
  patient;
  fullName;
  iconColor;
  initials;

  $onChanges(changes) {   
    if (changes.patient.currentValue){
      this.patient = changes.patient.currentValue;
      this.fullName = this.getFullName();
      this.initials = this.getInitials();
      this.iconColor = this.getIconColor();
    }
  }

  private getIconColor() {
    return (this.patient.sex === 'male') ? 'LightSkyBlue' : 'pink';
  }

  private getInitials() { 
    const {firstName, lastName} = this.patient;
    const getFirstSymbol = (item) => item[0];    
    return `${getFirstSymbol(firstName)}${getFirstSymbol(lastName)}`;       
  }

  private getFullName() {
    return `${this.patient.firstName} ${this.patient.lastName}`;
  }

}
