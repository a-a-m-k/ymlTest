
export class WidgetTableService {

  constructor(private $http: ng.IHttpService) {
    this.$http = $http;
  }

  getPatient() {
    return this.$http.get('widgets.json');
  }

  getGridOptions(widget, index, property, namebtn) {
    return { 
      enableCellEditOnFocus: true,
      enableHorizontalScrollbar: 0,
      enableVerticalScrollbar: 0,
      rowHeight: 119,
      enableSorting: false,
      enableHiding: false,
      columnDefs: this.getColumnDefs(widget, index, property, namebtn)
    };
  }

  getColumnDefs(widget, index, property, namebtn) {
    return [
      {name: `${widget}[${index}].date`, enableCellEdit: false, displayName: 'date', width: 85, cellClass: 'blue'},
      {name: `${widget}[${index}].${property}`, enableCellEditOnFocus: false, displayName: property +'(edit)', width: 150}
    ];
  } 
}

