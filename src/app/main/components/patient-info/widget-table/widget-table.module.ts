import * as angular from 'angular';
import { WidgetTableComponent } from './widget-table.component';
import { WidgetTableService } from './widget-table.service';
import { WidgetTableConstant } from './widget-table.constant';
import ngMaterial  = require('angular-material');
import uiGrid = require('angular-ui-grid');
import 'angular-material/angular-material.css';
import 'angular-ui-grid/ui-grid.css';

export const widgetTableModule = angular
  .module('app.main.widgetTable', [ ngMaterial, uiGrid, 'ui.grid.edit', 'ui.grid.cellNav'])
  .component('widgetTableComponent', WidgetTableComponent)
  .service('widgetTableService', ['$http', WidgetTableService])
  .constant('widgetTableConstant', WidgetTableConstant.constants)
  .name;
