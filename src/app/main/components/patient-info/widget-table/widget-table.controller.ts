import * as _ from 'lodash'; 
import { WidgetTableService } from './widget-table.service';
import { WidgetTableConstant } from './widget-table.constant';

export class WidgetTableController {
  static $inject = ['widgetTableService', 'widgetTableConstant'];

  gridOptions: any;
  private widget: string;
  private property: string;
  private namebtn: string = this.widgetTableConstant.previous;
  private index = 0;
  private widgetName: string;

  constructor(
    private widgetTableService: WidgetTableService,
    private widgetTableConstant: WidgetTableConstant
  ) {};
  
  $onInit() {
    this.widgetName = _.upperFirst(this.widget);
    this.gridOptions = this.widgetTableService.getGridOptions(this.widget, this.index, this.property, this.namebtn);

    this.widgetTableService.getPatient().then((response) => {
      this.gridOptions.data = [response.data];
    });
  }
  previousLatest() {
    this.namebtn = (this.namebtn === this.widgetTableConstant.previous) ? this.widgetTableConstant.latest : this.widgetTableConstant.previous;
    this.index = (this.namebtn === this.widgetTableConstant.latest) ? 1 : 0;
    this.gridOptions.columnDefs = this.widgetTableService.getColumnDefs(this.widget, this.index, this.property, this.namebtn);
  }
}

  


  

