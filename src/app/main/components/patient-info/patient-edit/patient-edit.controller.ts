import * as angular from 'angular';

export class PatientEditController {

  static $inject = ['$mdDialog'];

  constructor(    
    private $mdDialog: any
  ) {};

  showPatientEditModal(ev) {
    this.$mdDialog.show({
      contentElement: '#editDialog',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose: true
    });
  };

  hidePatientEditModal() {
    this.$mdDialog.cancel();
  };

  savePatientEditModal() {
    this.$mdDialog.cancel();
  };
}
