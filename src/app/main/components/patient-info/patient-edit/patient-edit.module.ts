import * as angular from 'angular';
import { PatientEditComponent } from './patient-edit.component';

export const patientEditModule = angular
  .module('app.main.patient-edit', [])
  .component('patientEditComponent', PatientEditComponent)
  .name;
