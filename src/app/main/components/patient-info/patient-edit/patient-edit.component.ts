import './patient-edit.component.scss';

import { PatientEditController } from './patient-edit.controller';

export const PatientEditComponent: angular.IComponentOptions = {
    bindings: {
        patientInfo: '<'
    },
    template: require('./patient-edit.component.html'),
    controller: PatientEditController
};
