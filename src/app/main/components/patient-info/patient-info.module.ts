import * as angular from 'angular';
import { PatientInfoComponent } from './patient-info.component';
import { PacientIconColor } from './patient-info.directive';
import { widgetTableModule } from './widget-table/widget-table.module';
import {patientEditModule } from './patient-edit/patient-edit.module';
export const patientInfoModule = angular
  .module('app.main.components.info', [ widgetTableModule,patientEditModule])
  .component('patientInfoComponent', PatientInfoComponent)
  .directive('pacientIconColor', PacientIconColor)
  .name;
