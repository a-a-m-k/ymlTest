import './menu.component.scss';

import { MenuController } from './menu.controller';

export const MenuComponent: angular.IComponentOptions = {
    template: require('./menu.component.html'),
    controller: MenuController
};
