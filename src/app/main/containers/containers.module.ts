import * as angular from 'angular';
import { patientsContainerModule } from './patients-container/patients-container.module';
import { viewportModule } from './viewport/viewport.module';

export const containersModule = angular
  .module('app.main.containers', [
    patientsContainerModule,
    viewportModule
  ])
  .name;