export class PatientsContainerService {
  constructor(private $http) {}

  public getPatients() {
    return this.$http.get('patients.json');
  }
}

