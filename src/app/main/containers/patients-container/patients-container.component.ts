import { PatientsContainerController } from './patients-container.controller';
import './patients-container.component.scss';

export const PatientsContainerComponent: angular.IComponentOptions = {
    template: require('./patients-container.component.html'),
    controller: PatientsContainerController
};
