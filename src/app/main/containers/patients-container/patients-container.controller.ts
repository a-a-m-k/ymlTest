import { PatientsContainerService } from './patients-container.service';

export class PatientsContainerController {
  static $inject = ['PatientsContainerService', '$timeout'];
  patients;
  selectedPatient;
  filteredPatients;

  constructor(
    private patientsContainerService: PatientsContainerService,
    private timeout
  ) { };

  $onInit() {
    this.getPatients();
  }

  private getPatients() {
    this.patientsContainerService.getPatients()
      .then(response => {
        this.patients = response.data;
        this.filteredPatients = response.data;
        this.selectedPatient = this.patients[0];        
      });
  }

  selectPatient(index) {
    this.selectedPatient = this.patients.find(patient => patient.id === index);
  }

  changeFilter(res) { 
    this.timeout(() => {
      this.filteredPatients = res;
    });
  }

}
