import * as angular from 'angular';
import { Viewport } from './viewport.component';

export const viewportModule = angular
  .module('app.main.containers.viewport', [])

  .component('viewport', Viewport)
  .name;
